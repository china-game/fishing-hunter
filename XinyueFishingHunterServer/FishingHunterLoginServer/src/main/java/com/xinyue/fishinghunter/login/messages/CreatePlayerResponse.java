package com.xinyue.fishinghunter.login.messages;

import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.CreatePlayerResponseParams;
import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1001, serviceId = 2, messageType = EnumMesasageType.RESPONSE, desc = "创建角色")
public class CreatePlayerResponse extends AbstractServiceGameMessage {

	private CreatePlayerResponseParams.Builder builder = CreatePlayerResponseParams.newBuilder();

	@Override
	public CreatePlayerResponseParams.Builder getMessageBuilder() {
		return builder;
	}

	@Override
	protected IGameMessage newCoupleImpl() {
		return new CreatePlayerRequest();
	}

}
