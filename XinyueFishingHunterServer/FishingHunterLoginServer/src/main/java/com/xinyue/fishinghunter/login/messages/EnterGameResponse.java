package com.xinyue.fishinghunter.login.messages;

import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.EnterGameResponseParams;
import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1004, serviceId = 2, messageType = EnumMesasageType.RESPONSE, desc = "进入游戏")
public class EnterGameResponse extends AbstractServiceGameMessage {
	private EnterGameResponseParams.Builder builder = EnterGameResponseParams.newBuilder();

	@Override
	public EnterGameResponseParams.Builder getMessageBuilder() {
		return builder;
	}

	@Override
	protected IGameMessage newCoupleImpl() {
		return new EnterGameRequest();
	}

}
