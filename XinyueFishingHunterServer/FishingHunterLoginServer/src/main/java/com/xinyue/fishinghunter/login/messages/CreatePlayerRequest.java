package com.xinyue.fishinghunter.login.messages;

import com.google.protobuf.InvalidProtocolBufferException;
import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.CreatePlayerRequestParams;
import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1001, serviceId = 2, messageType = EnumMesasageType.REQUEST, desc = "创建角色")
public class CreatePlayerRequest extends AbstractServiceGameMessage {
	private CreatePlayerRequestParams params;

	public CreatePlayerRequestParams getParams() {
		return params;
	}

	@Override
	protected void parseFrom(byte[] bytes) throws InvalidProtocolBufferException {
		params = CreatePlayerRequestParams.parseFrom(bytes);
	}

	@Override
	protected IGameMessage newCoupleImpl() {
		return new CreatePlayerResponse();
	}

}
