package com.xinyue.fishinghunter.login.messages;

import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1004, serviceId = 2, messageType = EnumMesasageType.REQUEST, desc = "进入游戏")
public class EnterGameRequest extends AbstractServiceGameMessage {

	@Override
	protected IGameMessage newCoupleImpl() {
		return new EnterGameResponse();
	}

}
