package com.xinyue.fishinghunter.event.messages;

import com.google.protobuf.InvalidProtocolBufferException;
import com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams;
import com.xinyue.mqsystem.event.AbstractEventMessage;
import com.xinyue.mqsystem.event.GameEventMetadata;

@GameEventMetadata(eventId = 1001, eventName = "创建昵称")
public class CreatePlayerMQEvent extends AbstractEventMessage {
	private CreatePlayerEventParams.Builder builder;
	private CreatePlayerEventParams params;

	@Override
	public CreatePlayerEventParams.Builder getMessageBuilder() {
		if (builder == null) {
			builder = CreatePlayerEventParams.newBuilder();
		}
		return builder;
	}

	@Override
	protected void parseFrom(byte[] bytes) throws InvalidProtocolBufferException {
		params = CreatePlayerEventParams.parseFrom(bytes);
	}

	public CreatePlayerEventParams getParams() {
		return params;
	}

}
