// Generated by the protocol buffer compiler.  DO NOT EDIT!
// source: FishingHunterMQEventMessages.proto

package com.xinyue.fishinghunter.event.messages;

public final class FishingHunterEvents {
  private FishingHunterEvents() {}
  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistryLite registry) {
  }

  public static void registerAllExtensions(
      com.google.protobuf.ExtensionRegistry registry) {
    registerAllExtensions(
        (com.google.protobuf.ExtensionRegistryLite) registry);
  }
  public interface CreatePlayerEventParamsOrBuilder extends
      // @@protoc_insertion_point(interface_extends:fishinghunter.CreatePlayerEventParams)
      com.google.protobuf.MessageOrBuilder {

    /**
     * <code>string accountId = 1;</code>
     * @return The accountId.
     */
    java.lang.String getAccountId();
    /**
     * <code>string accountId = 1;</code>
     * @return The bytes for accountId.
     */
    com.google.protobuf.ByteString
        getAccountIdBytes();

    /**
     * <code>int64 playerId = 2;</code>
     * @return The playerId.
     */
    long getPlayerId();

    /**
     * <code>string nickname = 3;</code>
     * @return The nickname.
     */
    java.lang.String getNickname();
    /**
     * <code>string nickname = 3;</code>
     * @return The bytes for nickname.
     */
    com.google.protobuf.ByteString
        getNicknameBytes();
  }
  /**
   * Protobuf type {@code fishinghunter.CreatePlayerEventParams}
   */
  public  static final class CreatePlayerEventParams extends
      com.google.protobuf.GeneratedMessageV3 implements
      // @@protoc_insertion_point(message_implements:fishinghunter.CreatePlayerEventParams)
      CreatePlayerEventParamsOrBuilder {
  private static final long serialVersionUID = 0L;
    // Use CreatePlayerEventParams.newBuilder() to construct.
    private CreatePlayerEventParams(com.google.protobuf.GeneratedMessageV3.Builder<?> builder) {
      super(builder);
    }
    private CreatePlayerEventParams() {
      accountId_ = "";
      nickname_ = "";
    }

    @java.lang.Override
    @SuppressWarnings({"unused"})
    protected java.lang.Object newInstance(
        UnusedPrivateParameter unused) {
      return new CreatePlayerEventParams();
    }

    @java.lang.Override
    public final com.google.protobuf.UnknownFieldSet
    getUnknownFields() {
      return this.unknownFields;
    }
    private CreatePlayerEventParams(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      this();
      if (extensionRegistry == null) {
        throw new java.lang.NullPointerException();
      }
      com.google.protobuf.UnknownFieldSet.Builder unknownFields =
          com.google.protobuf.UnknownFieldSet.newBuilder();
      try {
        boolean done = false;
        while (!done) {
          int tag = input.readTag();
          switch (tag) {
            case 0:
              done = true;
              break;
            case 10: {
              java.lang.String s = input.readStringRequireUtf8();

              accountId_ = s;
              break;
            }
            case 16: {

              playerId_ = input.readInt64();
              break;
            }
            case 26: {
              java.lang.String s = input.readStringRequireUtf8();

              nickname_ = s;
              break;
            }
            default: {
              if (!parseUnknownField(
                  input, unknownFields, extensionRegistry, tag)) {
                done = true;
              }
              break;
            }
          }
        }
      } catch (com.google.protobuf.InvalidProtocolBufferException e) {
        throw e.setUnfinishedMessage(this);
      } catch (java.io.IOException e) {
        throw new com.google.protobuf.InvalidProtocolBufferException(
            e).setUnfinishedMessage(this);
      } finally {
        this.unknownFields = unknownFields.build();
        makeExtensionsImmutable();
      }
    }
    public static final com.google.protobuf.Descriptors.Descriptor
        getDescriptor() {
      return com.xinyue.fishinghunter.event.messages.FishingHunterEvents.internal_static_fishinghunter_CreatePlayerEventParams_descriptor;
    }

    @java.lang.Override
    protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
        internalGetFieldAccessorTable() {
      return com.xinyue.fishinghunter.event.messages.FishingHunterEvents.internal_static_fishinghunter_CreatePlayerEventParams_fieldAccessorTable
          .ensureFieldAccessorsInitialized(
              com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams.class, com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams.Builder.class);
    }

    public static final int ACCOUNTID_FIELD_NUMBER = 1;
    private volatile java.lang.Object accountId_;
    /**
     * <code>string accountId = 1;</code>
     * @return The accountId.
     */
    public java.lang.String getAccountId() {
      java.lang.Object ref = accountId_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        accountId_ = s;
        return s;
      }
    }
    /**
     * <code>string accountId = 1;</code>
     * @return The bytes for accountId.
     */
    public com.google.protobuf.ByteString
        getAccountIdBytes() {
      java.lang.Object ref = accountId_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        accountId_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    public static final int PLAYERID_FIELD_NUMBER = 2;
    private long playerId_;
    /**
     * <code>int64 playerId = 2;</code>
     * @return The playerId.
     */
    public long getPlayerId() {
      return playerId_;
    }

    public static final int NICKNAME_FIELD_NUMBER = 3;
    private volatile java.lang.Object nickname_;
    /**
     * <code>string nickname = 3;</code>
     * @return The nickname.
     */
    public java.lang.String getNickname() {
      java.lang.Object ref = nickname_;
      if (ref instanceof java.lang.String) {
        return (java.lang.String) ref;
      } else {
        com.google.protobuf.ByteString bs = 
            (com.google.protobuf.ByteString) ref;
        java.lang.String s = bs.toStringUtf8();
        nickname_ = s;
        return s;
      }
    }
    /**
     * <code>string nickname = 3;</code>
     * @return The bytes for nickname.
     */
    public com.google.protobuf.ByteString
        getNicknameBytes() {
      java.lang.Object ref = nickname_;
      if (ref instanceof java.lang.String) {
        com.google.protobuf.ByteString b = 
            com.google.protobuf.ByteString.copyFromUtf8(
                (java.lang.String) ref);
        nickname_ = b;
        return b;
      } else {
        return (com.google.protobuf.ByteString) ref;
      }
    }

    private byte memoizedIsInitialized = -1;
    @java.lang.Override
    public final boolean isInitialized() {
      byte isInitialized = memoizedIsInitialized;
      if (isInitialized == 1) return true;
      if (isInitialized == 0) return false;

      memoizedIsInitialized = 1;
      return true;
    }

    @java.lang.Override
    public void writeTo(com.google.protobuf.CodedOutputStream output)
                        throws java.io.IOException {
      if (!getAccountIdBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 1, accountId_);
      }
      if (playerId_ != 0L) {
        output.writeInt64(2, playerId_);
      }
      if (!getNicknameBytes().isEmpty()) {
        com.google.protobuf.GeneratedMessageV3.writeString(output, 3, nickname_);
      }
      unknownFields.writeTo(output);
    }

    @java.lang.Override
    public int getSerializedSize() {
      int size = memoizedSize;
      if (size != -1) return size;

      size = 0;
      if (!getAccountIdBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(1, accountId_);
      }
      if (playerId_ != 0L) {
        size += com.google.protobuf.CodedOutputStream
          .computeInt64Size(2, playerId_);
      }
      if (!getNicknameBytes().isEmpty()) {
        size += com.google.protobuf.GeneratedMessageV3.computeStringSize(3, nickname_);
      }
      size += unknownFields.getSerializedSize();
      memoizedSize = size;
      return size;
    }

    @java.lang.Override
    public boolean equals(final java.lang.Object obj) {
      if (obj == this) {
       return true;
      }
      if (!(obj instanceof com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams)) {
        return super.equals(obj);
      }
      com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams other = (com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams) obj;

      if (!getAccountId()
          .equals(other.getAccountId())) return false;
      if (getPlayerId()
          != other.getPlayerId()) return false;
      if (!getNickname()
          .equals(other.getNickname())) return false;
      if (!unknownFields.equals(other.unknownFields)) return false;
      return true;
    }

    @java.lang.Override
    public int hashCode() {
      if (memoizedHashCode != 0) {
        return memoizedHashCode;
      }
      int hash = 41;
      hash = (19 * hash) + getDescriptor().hashCode();
      hash = (37 * hash) + ACCOUNTID_FIELD_NUMBER;
      hash = (53 * hash) + getAccountId().hashCode();
      hash = (37 * hash) + PLAYERID_FIELD_NUMBER;
      hash = (53 * hash) + com.google.protobuf.Internal.hashLong(
          getPlayerId());
      hash = (37 * hash) + NICKNAME_FIELD_NUMBER;
      hash = (53 * hash) + getNickname().hashCode();
      hash = (29 * hash) + unknownFields.hashCode();
      memoizedHashCode = hash;
      return hash;
    }

    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        java.nio.ByteBuffer data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        java.nio.ByteBuffer data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        com.google.protobuf.ByteString data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        com.google.protobuf.ByteString data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(byte[] data)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        byte[] data,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws com.google.protobuf.InvalidProtocolBufferException {
      return PARSER.parseFrom(data, extensionRegistry);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseDelimitedFrom(java.io.InputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseDelimitedFrom(
        java.io.InputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseDelimitedWithIOException(PARSER, input, extensionRegistry);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        com.google.protobuf.CodedInputStream input)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input);
    }
    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parseFrom(
        com.google.protobuf.CodedInputStream input,
        com.google.protobuf.ExtensionRegistryLite extensionRegistry)
        throws java.io.IOException {
      return com.google.protobuf.GeneratedMessageV3
          .parseWithIOException(PARSER, input, extensionRegistry);
    }

    @java.lang.Override
    public Builder newBuilderForType() { return newBuilder(); }
    public static Builder newBuilder() {
      return DEFAULT_INSTANCE.toBuilder();
    }
    public static Builder newBuilder(com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams prototype) {
      return DEFAULT_INSTANCE.toBuilder().mergeFrom(prototype);
    }
    @java.lang.Override
    public Builder toBuilder() {
      return this == DEFAULT_INSTANCE
          ? new Builder() : new Builder().mergeFrom(this);
    }

    @java.lang.Override
    protected Builder newBuilderForType(
        com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
      Builder builder = new Builder(parent);
      return builder;
    }
    /**
     * Protobuf type {@code fishinghunter.CreatePlayerEventParams}
     */
    public static final class Builder extends
        com.google.protobuf.GeneratedMessageV3.Builder<Builder> implements
        // @@protoc_insertion_point(builder_implements:fishinghunter.CreatePlayerEventParams)
        com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParamsOrBuilder {
      public static final com.google.protobuf.Descriptors.Descriptor
          getDescriptor() {
        return com.xinyue.fishinghunter.event.messages.FishingHunterEvents.internal_static_fishinghunter_CreatePlayerEventParams_descriptor;
      }

      @java.lang.Override
      protected com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
          internalGetFieldAccessorTable() {
        return com.xinyue.fishinghunter.event.messages.FishingHunterEvents.internal_static_fishinghunter_CreatePlayerEventParams_fieldAccessorTable
            .ensureFieldAccessorsInitialized(
                com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams.class, com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams.Builder.class);
      }

      // Construct using com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams.newBuilder()
      private Builder() {
        maybeForceBuilderInitialization();
      }

      private Builder(
          com.google.protobuf.GeneratedMessageV3.BuilderParent parent) {
        super(parent);
        maybeForceBuilderInitialization();
      }
      private void maybeForceBuilderInitialization() {
        if (com.google.protobuf.GeneratedMessageV3
                .alwaysUseFieldBuilders) {
        }
      }
      @java.lang.Override
      public Builder clear() {
        super.clear();
        accountId_ = "";

        playerId_ = 0L;

        nickname_ = "";

        return this;
      }

      @java.lang.Override
      public com.google.protobuf.Descriptors.Descriptor
          getDescriptorForType() {
        return com.xinyue.fishinghunter.event.messages.FishingHunterEvents.internal_static_fishinghunter_CreatePlayerEventParams_descriptor;
      }

      @java.lang.Override
      public com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams getDefaultInstanceForType() {
        return com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams.getDefaultInstance();
      }

      @java.lang.Override
      public com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams build() {
        com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams result = buildPartial();
        if (!result.isInitialized()) {
          throw newUninitializedMessageException(result);
        }
        return result;
      }

      @java.lang.Override
      public com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams buildPartial() {
        com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams result = new com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams(this);
        result.accountId_ = accountId_;
        result.playerId_ = playerId_;
        result.nickname_ = nickname_;
        onBuilt();
        return result;
      }

      @java.lang.Override
      public Builder clone() {
        return super.clone();
      }
      @java.lang.Override
      public Builder setField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.setField(field, value);
      }
      @java.lang.Override
      public Builder clearField(
          com.google.protobuf.Descriptors.FieldDescriptor field) {
        return super.clearField(field);
      }
      @java.lang.Override
      public Builder clearOneof(
          com.google.protobuf.Descriptors.OneofDescriptor oneof) {
        return super.clearOneof(oneof);
      }
      @java.lang.Override
      public Builder setRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          int index, java.lang.Object value) {
        return super.setRepeatedField(field, index, value);
      }
      @java.lang.Override
      public Builder addRepeatedField(
          com.google.protobuf.Descriptors.FieldDescriptor field,
          java.lang.Object value) {
        return super.addRepeatedField(field, value);
      }
      @java.lang.Override
      public Builder mergeFrom(com.google.protobuf.Message other) {
        if (other instanceof com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams) {
          return mergeFrom((com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams)other);
        } else {
          super.mergeFrom(other);
          return this;
        }
      }

      public Builder mergeFrom(com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams other) {
        if (other == com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams.getDefaultInstance()) return this;
        if (!other.getAccountId().isEmpty()) {
          accountId_ = other.accountId_;
          onChanged();
        }
        if (other.getPlayerId() != 0L) {
          setPlayerId(other.getPlayerId());
        }
        if (!other.getNickname().isEmpty()) {
          nickname_ = other.nickname_;
          onChanged();
        }
        this.mergeUnknownFields(other.unknownFields);
        onChanged();
        return this;
      }

      @java.lang.Override
      public final boolean isInitialized() {
        return true;
      }

      @java.lang.Override
      public Builder mergeFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws java.io.IOException {
        com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams parsedMessage = null;
        try {
          parsedMessage = PARSER.parsePartialFrom(input, extensionRegistry);
        } catch (com.google.protobuf.InvalidProtocolBufferException e) {
          parsedMessage = (com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams) e.getUnfinishedMessage();
          throw e.unwrapIOException();
        } finally {
          if (parsedMessage != null) {
            mergeFrom(parsedMessage);
          }
        }
        return this;
      }

      private java.lang.Object accountId_ = "";
      /**
       * <code>string accountId = 1;</code>
       * @return The accountId.
       */
      public java.lang.String getAccountId() {
        java.lang.Object ref = accountId_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          accountId_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>string accountId = 1;</code>
       * @return The bytes for accountId.
       */
      public com.google.protobuf.ByteString
          getAccountIdBytes() {
        java.lang.Object ref = accountId_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          accountId_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>string accountId = 1;</code>
       * @param value The accountId to set.
       * @return This builder for chaining.
       */
      public Builder setAccountId(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        accountId_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>string accountId = 1;</code>
       * @return This builder for chaining.
       */
      public Builder clearAccountId() {
        
        accountId_ = getDefaultInstance().getAccountId();
        onChanged();
        return this;
      }
      /**
       * <code>string accountId = 1;</code>
       * @param value The bytes for accountId to set.
       * @return This builder for chaining.
       */
      public Builder setAccountIdBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        accountId_ = value;
        onChanged();
        return this;
      }

      private long playerId_ ;
      /**
       * <code>int64 playerId = 2;</code>
       * @return The playerId.
       */
      public long getPlayerId() {
        return playerId_;
      }
      /**
       * <code>int64 playerId = 2;</code>
       * @param value The playerId to set.
       * @return This builder for chaining.
       */
      public Builder setPlayerId(long value) {
        
        playerId_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>int64 playerId = 2;</code>
       * @return This builder for chaining.
       */
      public Builder clearPlayerId() {
        
        playerId_ = 0L;
        onChanged();
        return this;
      }

      private java.lang.Object nickname_ = "";
      /**
       * <code>string nickname = 3;</code>
       * @return The nickname.
       */
      public java.lang.String getNickname() {
        java.lang.Object ref = nickname_;
        if (!(ref instanceof java.lang.String)) {
          com.google.protobuf.ByteString bs =
              (com.google.protobuf.ByteString) ref;
          java.lang.String s = bs.toStringUtf8();
          nickname_ = s;
          return s;
        } else {
          return (java.lang.String) ref;
        }
      }
      /**
       * <code>string nickname = 3;</code>
       * @return The bytes for nickname.
       */
      public com.google.protobuf.ByteString
          getNicknameBytes() {
        java.lang.Object ref = nickname_;
        if (ref instanceof String) {
          com.google.protobuf.ByteString b = 
              com.google.protobuf.ByteString.copyFromUtf8(
                  (java.lang.String) ref);
          nickname_ = b;
          return b;
        } else {
          return (com.google.protobuf.ByteString) ref;
        }
      }
      /**
       * <code>string nickname = 3;</code>
       * @param value The nickname to set.
       * @return This builder for chaining.
       */
      public Builder setNickname(
          java.lang.String value) {
        if (value == null) {
    throw new NullPointerException();
  }
  
        nickname_ = value;
        onChanged();
        return this;
      }
      /**
       * <code>string nickname = 3;</code>
       * @return This builder for chaining.
       */
      public Builder clearNickname() {
        
        nickname_ = getDefaultInstance().getNickname();
        onChanged();
        return this;
      }
      /**
       * <code>string nickname = 3;</code>
       * @param value The bytes for nickname to set.
       * @return This builder for chaining.
       */
      public Builder setNicknameBytes(
          com.google.protobuf.ByteString value) {
        if (value == null) {
    throw new NullPointerException();
  }
  checkByteStringIsUtf8(value);
        
        nickname_ = value;
        onChanged();
        return this;
      }
      @java.lang.Override
      public final Builder setUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.setUnknownFields(unknownFields);
      }

      @java.lang.Override
      public final Builder mergeUnknownFields(
          final com.google.protobuf.UnknownFieldSet unknownFields) {
        return super.mergeUnknownFields(unknownFields);
      }


      // @@protoc_insertion_point(builder_scope:fishinghunter.CreatePlayerEventParams)
    }

    // @@protoc_insertion_point(class_scope:fishinghunter.CreatePlayerEventParams)
    private static final com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams DEFAULT_INSTANCE;
    static {
      DEFAULT_INSTANCE = new com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams();
    }

    public static com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams getDefaultInstance() {
      return DEFAULT_INSTANCE;
    }

    private static final com.google.protobuf.Parser<CreatePlayerEventParams>
        PARSER = new com.google.protobuf.AbstractParser<CreatePlayerEventParams>() {
      @java.lang.Override
      public CreatePlayerEventParams parsePartialFrom(
          com.google.protobuf.CodedInputStream input,
          com.google.protobuf.ExtensionRegistryLite extensionRegistry)
          throws com.google.protobuf.InvalidProtocolBufferException {
        return new CreatePlayerEventParams(input, extensionRegistry);
      }
    };

    public static com.google.protobuf.Parser<CreatePlayerEventParams> parser() {
      return PARSER;
    }

    @java.lang.Override
    public com.google.protobuf.Parser<CreatePlayerEventParams> getParserForType() {
      return PARSER;
    }

    @java.lang.Override
    public com.xinyue.fishinghunter.event.messages.FishingHunterEvents.CreatePlayerEventParams getDefaultInstanceForType() {
      return DEFAULT_INSTANCE;
    }

  }

  private static final com.google.protobuf.Descriptors.Descriptor
    internal_static_fishinghunter_CreatePlayerEventParams_descriptor;
  private static final 
    com.google.protobuf.GeneratedMessageV3.FieldAccessorTable
      internal_static_fishinghunter_CreatePlayerEventParams_fieldAccessorTable;

  public static com.google.protobuf.Descriptors.FileDescriptor
      getDescriptor() {
    return descriptor;
  }
  private static  com.google.protobuf.Descriptors.FileDescriptor
      descriptor;
  static {
    java.lang.String[] descriptorData = {
      "\n\"FishingHunterMQEventMessages.proto\022\rfi" +
      "shinghunter\"P\n\027CreatePlayerEventParams\022\021" +
      "\n\taccountId\030\001 \001(\t\022\020\n\010playerId\030\002 \001(\003\022\020\n\010n" +
      "ickname\030\003 \001(\tB>\n\'com.xinyue.fishinghunte" +
      "r.event.messagesB\023FishingHunterEventsb\006p" +
      "roto3"
    };
    descriptor = com.google.protobuf.Descriptors.FileDescriptor
      .internalBuildGeneratedFileFrom(descriptorData,
        new com.google.protobuf.Descriptors.FileDescriptor[] {
        });
    internal_static_fishinghunter_CreatePlayerEventParams_descriptor =
      getDescriptor().getMessageTypes().get(0);
    internal_static_fishinghunter_CreatePlayerEventParams_fieldAccessorTable = new
      com.google.protobuf.GeneratedMessageV3.FieldAccessorTable(
        internal_static_fishinghunter_CreatePlayerEventParams_descriptor,
        new java.lang.String[] { "AccountId", "PlayerId", "Nickname", });
  }

  // @@protoc_insertion_point(outer_class_scope)
}
