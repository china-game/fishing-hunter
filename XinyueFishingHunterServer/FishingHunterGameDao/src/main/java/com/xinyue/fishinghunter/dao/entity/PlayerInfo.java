package com.xinyue.fishinghunter.dao.entity;

public class PlayerInfo {

	private long playerId;
	private String nickname;

	public long getPlayerId() {
		return playerId;
	}

	public void setPlayerId(long playerId) {
		this.playerId = playerId;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

}
