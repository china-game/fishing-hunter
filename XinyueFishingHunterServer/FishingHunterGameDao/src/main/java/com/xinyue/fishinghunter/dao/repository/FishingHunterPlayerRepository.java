package com.xinyue.fishinghunter.dao.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.xinyue.fishinghunter.dao.entity.FishingHunterPlayer;

public interface FishingHunterPlayerRepository extends MongoRepository<FishingHunterPlayer, Long>{

}
