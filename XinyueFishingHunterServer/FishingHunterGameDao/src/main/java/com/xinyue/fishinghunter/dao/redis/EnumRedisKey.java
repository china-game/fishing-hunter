package com.xinyue.fishinghunter.dao.redis;

import java.time.Duration;

import com.xinyue.framework.dao.redis.IRedisKeyConfig;

public enum EnumRedisKey implements IRedisKeyConfig{

	PLAYER_INFO(Duration.ofDays(7)),
	RANK(null),
	
	;
	private Duration duration;
	
	public static String Namespace = "defaultNameSpace";
	
	private EnumRedisKey(Duration duration) {
		this.duration = duration;
	}
	@Override
	public String getKey(String id) {
		return this.getKey() + "_" + id;
	}

	@Override
	public String getKey() {
		return Namespace + ":" + this.name().toLowerCase();
	}

	@Override
	public Duration getExpire() {
		return duration;
	}

}
