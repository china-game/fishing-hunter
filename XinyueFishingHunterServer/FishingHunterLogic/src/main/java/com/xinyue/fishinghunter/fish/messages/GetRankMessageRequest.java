package com.xinyue.fishinghunter.fish.messages;

import com.google.protobuf.InvalidProtocolBufferException;
import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.GetRankRequestParams;
import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1002, messageType = EnumMesasageType.REQUEST, serviceId = 1, desc = "获取排行榜数据")
public class GetRankMessageRequest extends AbstractServiceGameMessage {

	private FishingHunterMessages.GetRankRequestParams params;
	
	@Override
	protected void parseFrom(byte[] bytes) throws InvalidProtocolBufferException {
		params = FishingHunterMessages.GetRankRequestParams.parseFrom(bytes);
	}
	
	public GetRankRequestParams getParams() {
		return params;
	}
	
	@Override
	protected IGameMessage newCoupleImpl() {
		return new GetRankMessageResponse();
	}



}
