package com.xinyue.fishinghunter.fish.messages;

import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.UpdatePlayerResponseParams;
import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;
@GameMessageMetadata(messageId = 1003, messageType = EnumMesasageType.RESPONSE, serviceId = 1, desc = "更新player返回")
public class UpdatePlayerMessageResponse extends AbstractServiceGameMessage {

	private UpdatePlayerResponseParams.Builder builder = UpdatePlayerResponseParams.newBuilder();
	
	
	@Override
	public UpdatePlayerResponseParams.Builder getMessageBuilder() {
		return builder;
	}
	
	@Override
	protected IGameMessage newCoupleImpl() {
		
		return new UpdatePlayerMessageRequest();
	}



}
