package com.xinyue.fishinghunter.fish.service.rank;

import java.util.Set;

public interface IGameRank {

	int updateRank(String member, long value);

	int getRank(String member);

	/**
	 * 获取前n名的排行榜
	 * 
	 * @param count
	 * @return
	 */
	Set<RankInfo> getRankList(int count);
}
