package com.xinyue.fishinghunter.fish.service.rank;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class RankService {

	@Autowired
	private StringRedisTemplate redisTemplate;
	private Map<EnumRankType, IGameRank> gameRankMap = new HashMap<>();

	@PostConstruct
	public void init() {
		
		IGameRank scoreRank = new GameRank(redisTemplate, EnumRankType.ScoreRank);
		gameRankMap.put(EnumRankType.ScoreRank, scoreRank);
	}

	private IGameRank getGameRank(EnumRankType rankType) {
		IGameRank gameRank = this.gameRankMap.get(rankType);
		if (gameRank == null) {
			throw new NullPointerException("不存在排行榜：" + rankType);
		}
		return gameRank;
	}

	public int updateRank(long playerId, long score, EnumRankType rankType) {
		IGameRank gameRank = this.getGameRank(rankType);
		return gameRank.updateRank(String.valueOf(playerId), score);
	}

	public Set<RankInfo> getRankInfos(EnumRankType rankType,int count) {
		IGameRank gameRank = this.getGameRank(rankType);
		return gameRank.getRankList(count);
	}
	
	public int getMyRank(long playerId,EnumRankType rankType) {
		IGameRank gameRank = this.getGameRank(rankType);
		int myrank  =  gameRank.getRank(String.valueOf(playerId));
		if(myrank >= 0) {
			myrank ++;
		}
		return myrank;
	}
}
