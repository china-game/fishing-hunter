package com.xinyue.fishinghunter.fish.messages;

import com.xinyue.fishinghunter.fish.messages.FishingHunterMessages.GetRankResponseParams;
import com.xinyue.network.message.stream.AbstractServiceGameMessage;
import com.xinyue.network.message.stream.EnumMesasageType;
import com.xinyue.network.message.stream.GameMessageMetadata;
import com.xinyue.network.message.stream.IGameMessage;

@GameMessageMetadata(messageId = 1002, messageType = EnumMesasageType.RESPONSE, serviceId = 1, desc = "获取排行榜数据")
public class GetRankMessageResponse extends AbstractServiceGameMessage {

	private GetRankResponseParams.Builder builder = GetRankResponseParams.newBuilder();
	
	@Override
	public GetRankResponseParams.Builder getMessageBuilder() {
		return builder;
	}
	
	@Override
	protected IGameMessage newCoupleImpl() {
		return new GetRankMessageRequest();
	}


	

	

}
