package com.xinyue.fishinghunter.fish;

import com.xinyue.network.message.error.IServerError;

public enum FishServerErrorCode implements IServerError{
	PLAYER_NOT_EXIST(1001,"角色未创建"),
	RANK_TYLE_ERROR(1002,"排行榜类型错误"),
	;
	private int errorCode;
	private String errorDesc;

	private FishServerErrorCode(int errorCode,String errorDesc) {
		this.errorCode = errorCode;
		this.errorDesc = errorDesc;
	}
	@Override
	public int getErrorCode() {
		return errorCode;
	}

	@Override
	public String getErrorDesc() {
		return errorDesc;
	}

}
