package com.xinyue.fishinghunter.fish;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

import com.xinyue.game.logic.frame.XinyueGameServerBoot;

@SpringBootApplication
@ComponentScan(basePackages = "com.xinyue.fishinghunter")
@EnableMongoRepositories(basePackages = { "com.xinyue.fishinghunter.dao.repository" })
public class FishServerMain {

	public static void main(String[] args) {

		ApplicationContext context = SpringApplication.run(FishServerMain.class, args);
		XinyueGameServerBoot.run(context, args);

	}

}
