package com.xinyue.fishinghunter.fish.service.rank;

public enum EnumRankType {

	ScoreRank(1),
	
	;
	private int type;
	
	private EnumRankType(int type) {
		this.type = type;
	}
	
	public int getType() {
		return type;
	}
	
	public static EnumRankType getRankType(int type) {
		for(EnumRankType rankType : EnumRankType.values()) {
			if(rankType.getType() == type) {
				return rankType;
			}
		}
		return null;
	}
}
