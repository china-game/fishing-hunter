package com.xinyue.fishinghunter.fish.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.xinyue.fishinghunter.dao.FishingHunterPlayerDao;
import com.xinyue.fishinghunter.dao.entity.FishingHunterPlayer;
@Service
public class PlayerService  {

	@Autowired
	private FishingHunterPlayerDao playerDao;
	
	public Optional<FishingHunterPlayer> getPlayer(long playerId){
		return  playerDao.findByIdFromCacheOrLoader(playerId);
	}
	public void updatePlayer(FishingHunterPlayer player) {
		playerDao.saveOrUpdate(player, player.getPlayerId());
	}
	
	
}
