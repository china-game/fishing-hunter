
# fishing-hunter

## 介绍
一款小捕鱼游戏，休闲娱乐一下。主要用于技术学习与实践，本项目是基于<心悦游戏开发框架>开发，地址：[https://gitee.com/wgslucky/xinyue-game-frame](https://gitee.com/wgslucky/xinyue-game-frame) 是对心悦游戏框架的一次实际应用练习。

## 版本信息

1. JDK统一使用java 8 版本
2. Rocketmq 使用4.6.1版本
3. Nacos使用1.2.1 版本
4. unity3d使用2019.3.1f版本

## 游戏框架源码

#### 项目安装

1. 下载地址：https://gitee.com/wgslucky/xinyue-game-frame

2. 安装Eclipse和配置Maven

3. 导入到Eclipse，以方便查看代码

4. 将框架源码安装到本地仓库

   在框架源码项目`game-frame-server`下执行下面的命令：

   ```
   mvn clean install
   ```

   或在eclipse中，点击项目右键，Run as -> Maven install

#### 项目介绍

 1. game-frame-client

    这是客户端unity3d用到的框架代码。

 2.  game-frame-server-web

     这下面是两个web项目，`xinyue-game-web-gateway`是web服务的网关项目，用于对web服务的负载。

     `xinyue-game-center`是游戏服务中心项目，负责用户的注册，登陆，sdk判断，充值回调，公告等公共服务。后期也可以根据项目需要，将一些web服务单独拆分出现，独立部署。

 3.  xinyue-game-gateway

     这个是游戏长连接网关，用于和客户端直联，负责连接的认证，消息加密解密，解压，压缩，消息转发，负载等。

## 游戏示例项目源码

#### 项目安装

1. 下载地址：https://gitee.com/wgslucky/fishing-hunter

2. 导入到Eclipse之中

3. 在根项目XinyueFishingHunterServer下执行下面的命令

   ```
   mvn clean install
   ```

   或在eclipse中，点击项目右键，Run as -> Maven install

#### 项目介绍

1. FishingHunterLoginServer

   角色登陆服务，负责将角色的数据从数据库加载到redis，并返回给客户端

2. FishingHunterLogic

   游戏业务服务。根据游戏服务将来的需求，可以将一些模块单独拆出来，做为独立的服务部署，比如排行榜。

   此项目下的prtobuf文件夹中包括了用到的.proto文件和工具。

3. FishingHunterGameDao

   游戏的数据库连接与服务信息。单独做为一个项目，是因为可能会有多个业务模块引用它。

4. FishingHunterEventMessage

   服务间事件系统，用于服务之间数据更新的通知。单独做为一个项目，是因为可能会有多个业务模块引用它。



### 项目依赖关系

游戏示例项目会依赖于游戏框架项目，因为框架项目没有上传到公共的maven仓库，所以如果直接编译游戏示例项目，是编译不通过的，因为它会找不到依赖的游戏框架的jar包。所以在编译游戏示例项目之前，需要先编译游戏架构项目。编译步骤就是上面所说的`游戏框架源码`部分。别外，因为xinyue-game-center项目依赖了FishingHunterEventMessage项目，所以xinyue-game-center项目的编译需要放在FishingHunterEventMessage编译之后。所以，请下面的步骤将项目发布到本的maven仓库中心之中。
1. 在框架源码项目`game-frame-server`下执行下面的命令：

   ```
   mvn clean install
   ```
2. 在根项目XinyueFishingHunterServer下执行下面的命令

   ```
   mvn clean install
   ```
3. 在其它项目pom所在的目录执行

   ```
   mvn clean install
   ```

### 客户端源码

https://gitee.com/wgslucky/xinyue-fish

## Nacos服务安装

1. 下载安装包：https://github.com/alibaba/nacos/releases  
2. 选择合适的安装包，这里选择：[nacos-server-1.2.1.tar.gz](https://github.com/alibaba/nacos/releases/download/1.2.1/nacos-server-1.2.1.tar.gz)，如果是windows环境，下载zip的安装包。
3. 解压下载的压缩包
4. 进入../nacos/bin目录，运行命令

 ```
sh startup.sh -m standalone
 ```

输出以下信息，表示启动成功：

```
nacos is starting with standalone
nacos is starting，you can check the /Users/topjoy/Desktop/wgs/nacos/logs/start.out
```

## RocketMQ安装

1. 下载安装包：https://github.com/apache/rocketmq/releases

2. 选择rocketmq-all-4.6.1版本

3. 解压下载的运行包

4. 启动rocketmq服务

   * 启动NameServer服务

     进入到rocketmq的bin目录，执行：

     ```
     sh mqnamesrv start
     ```

   * 启动Broker服务

     进入到rocketmq的bin目录，执行：

     ```
     sh mqbroker -n localhost:9876
     ```

   注意，以这种方式启动的rocketmq是不能关闭启动窗口的，关闭之后服务就停止了，正式环境下可以使用后台运行的方式启动。
   更多RocketMQ的使用，请参考RocketMQ文档：http://www.xinyues.com/col.jsp?id=103

## Redis服务安装

请自行按照网上的教程安装Redis服务

## MongoDB数据库安装

请自行按照网上的教程安装MongoDB

## 使用nacos管理平台

### 登陆nacos服务

启动nacos服务成功之后，在浏览器中输入：http://localhost:8848/nacos 即可打开nacos的登陆界面，默认的用户名和密码都是nacos。登陆成功之后，就可以对nacos进行管理了。

### 创建本地环境命名空间

登陆nacos之后，点击左侧菜单的`Namespace`项，然后再点击右上角的`Create Namespace`，输入相关的参数信息：

![创建命名空间](https://images.gitee.com/uploads/images/2020/0530/180732_31032c87_23677.png "image-20200529221639138.png")

点击OK之后，就成功创建了本地的开发环境命名空间了。然后在命名空间列表中就可以看到刚才创建的Namespace。点击右边的`Details`，可以看到有一个ID项，项目配置文件中配置的namespace就是这个ID:

![Namespace ID](https://images.gitee.com/uploads/images/2020/0530/180832_97e8de30_23677.png "image-20200529221917731.png")

### 创建公共配置文件

在分布式项目中，有很多配置文件，不同的项目可能引用的是同一份配置，如果每个项目都配置一次的话，不仅重复配置工作量大，而且更新维护也是一件费时的事情，如果每次都是人工修改，也难免会出错。所以可以把内容相同的配置文件创建成公共的配置文件，哪个项目用到这个配置文件，就直接引用文件名即可。

本项目使用了阿里的spring cloud 组件：spring-cloud-alibaba，在nacos中就可以管理分布式项目的配置文件内容，它代替了原来的spring cloud config（配置中心）和eureka（服务发现与注册中心）。

在nacoas中添加配置文件时需要注意的一个是dataId的命令，它的格式是：

```
${prefix}-${spring.profile.active}.${file-extension}
```

* prefix ：配置文件前缀，默认是spring.application.name的值，也可以通过配置spring.cloud.nacos.config.prefix的值指定。
* spring.profile.active : 当前配置环境需要激活的配置文件名。
* file-exetension : 配置文件扩展名，常用的有.properties和yaml。

在本项目中，公共配置的dataId格式示例：xinyue-game-redis-config.yaml，其中xinyue-game是prefix，redis-config是spring.profile.active，.yaml是文件扩展名。

#### 创建Redis配置信息

Redis服务请自行安装，配置好密码并启动即可。然后将Redis的信息配置到nacos之中，这样只需要配置一次，多个项目都可以同时引用了。

登陆nacos之后，点击左侧的菜单项：`ConfigManagerment->Configuraions`，再选择已创建的相应命名空间，比如local-env，然后点击右侧的"+"号，添加配置文件信息，由于项目使用的配置文件格式是YAML，所以这里选择Format为YAML。

![](https://images.gitee.com/uploads/images/2020/0530/181034_2159e23d_23677.png "image-20200530170150205.png")

然点击右下角的Publish按键即可创建成功，然后返回列表界面，就可以看到刚才创建成功的redis-config项了，如果想修改配置内容，可以点击右边的`Edit`进地编辑更新。

#### 创建MongoDB配置信息

MongoDB服务请自行安装，配置好密码并启动即可。然后将MongoDB的信息配置到nacos之中，这样只需要配置一次，多个项目都可以同时引用了。

登陆nacos之后，点击左侧的菜单项：`ConfigManagerment->Configuraions`，再选择已创建的相应命名空间，比如local-env，然后点击右侧的"+"号，添加配置文件信息，由于项目使用的配置文件格式是YAML，所以这里选择Format为YAML。

![](https://images.gitee.com/uploads/images/2020/0530/181120_ec965eff_23677.png "image-20200530170335909.png")

然点击右下角的Publish按键即可创建成功，然后返回列表界面，就可以看到刚才创建成功的mongo-config项了，如果想修改配置内容，可以点击右边的`Edit`进地编辑更新。

#### 创建Nacos的配置信息

因为在整个系统架构中，nacos做为服务发现与注册中心，所以项目需要配置nacos的信息。

登陆nacos之后，点击左侧的菜单项：`ConfigManagerment->Configuraions`，再选择已创建的相应命名空间，比如local-env，然后点击右侧的"+"号，添加配置文件信息，由于项目使用的配置文件格式是YAML，所以这里选择Format为YAML。

![](https://images.gitee.com/uploads/images/2020/0530/181151_1581e98f_23677.png "image-20200530170507246.png")

然点击右下角的Publish按键即可创建成功，然后返回列表界面，就可以看到刚才创建成功的nacos-config项了，如果想修改配置内容，可以点击右边的`Edit`进地编辑更新。

#### 创建RocketMQ配置信息

RocketMQ是做为整个系统内部消息通信的基础，安装方式见RocketMQ安装篇。

登陆nacos之后，点击左侧的菜单项：`ConfigManagerment->Configuraions`，再选择已创建的相应命名空间，比如local-env，然后点击右侧的"+"号，添加配置文件信息，由于项目使用的配置文件格式是YAML，所以这里选择Format为YAML。

![](https://images.gitee.com/uploads/images/2020/0530/181218_baae511f_23677.png "image-20200530170610282.png")

然点击右下角的Publish按键即可创建成功，然后返回列表界面，就可以看到刚才创建成功的rocketmq-config项了，如果想修改配置内容，可以点击右边的`Edit`进地编辑更新。

注意，以上配置信息需要配置成自己当前使用的系统信息。

#### 测试配置信息是否成功

可以使用下面的代码，测试配置是否成功

```java
public class NacosConfigService {

	public static void main(String[] args) throws NacosException, InterruptedException {
		String serverAddr = "localhost:8848";
		String dataId = "xinyue-game-redis-config.yaml";
		String group = "xinyue-game";
		Properties properties = new Properties();
		properties.put(PropertyKeyConst.SERVER_ADDR, serverAddr);
		properties.put(PropertyKeyConst.NAMESPACE, "local-env");

		ConfigService configService = NacosFactory.createConfigService(properties);
		String content = configService.getConfig(dataId, group, 5000);
		System.out.println(content);
	}
}
```



## 游戏示例项目添加配置

#### 游戏服务中心项目

在Eclipse中，打开xinyue-game-center项目的config/bootstrap.yml，添加nacos的服务信息与信息配置

```yaml
logging:
  config: file:config/log4j2.xml
server:
  port: 5003
spring:
  application:
    name: game-center-server
  profiles:
    active:   # 配置需要激活加载的文件名
     - rocketmq-config   
     - nacos-config
     - redis-config
     - mongo-config
  cloud:
    nacos:
      config:
        group: xinyue-game    # 在nacos中创建配置时的group名字
        file-extension: yaml  # 指定远程配置文件格式
        namespace: local-env # 配置所在的命名空间
        prefix: xinyue-game   # 配置文件前缀，默认是项目名称,它和上面active的文件名共同组成nacos配置的dataId
        server-addr: localhost:8848  # nacos的地址
```

在服务器启动的时候，根据上面的配置，就会拼成拉取的配置的dataId为，如下面的日志所示：

```java
2020-05-30 17:14:51 INFO  org.springframework.cloud.bootstrap.config.PropertySourceBootstrapConfiguration - Located property source: CompositePropertySource {name='NACOS', propertySources=[NacosPropertySource {name='xinyue-game-mongo-config.yaml,xinyue-game'}, NacosPropertySource {name='xinyue-game-redis-config.yaml,xinyue-game'}, NacosPropertySource {name='xinyue-game-nacos-config.yaml,xinyue-game'}, NacosPropertySource {name='xinyue-game-rocketmq-config.yaml,xinyue-game'}, NacosPropertySource {name='xinyue-game.yaml,xinyue-game'}, NacosPropertySource {name='xinyue-game,xinyue-game'}]}
2020-05-30 17:15:01 INFO  org.springframework.boot.SpringApplication - The following profiles are active: rocketmq-config,nacos-config,redis-config,mongo-config

```

application.yml的配置默认可以不用修改，端口修改可以根据自己的需要进行修改。

下面的配置信息都和此配置类似。

#### Web网关服务

在Eclipse中，打开xinyue-game-web-gateway项目的config/bootstrap.yml，添加nacos的服务信息与信息配置

```

spring:
  profiles:
    active:
    - nacos-config  # 因为网关不需要数据库等其它的配置，只激活nacos配置即可。

  cloud:
    nacos:
      config:
        group: xinyue-game
        file-extension: yaml  # 指定远程配置文件格式
        namespace: local-env
        prefix: xinyue-game
        server-addr: localhost:8848
```

application.yml 配置可以根据业务需要自行修改，默认情况下可以不用修改。

#### 游戏网关服务

在Eclipse中，打开xinyue-game-gateway项目的config/bootstrap.yml，添加nacos的服务信息与信息配置

```

spring:
  profiles:
    active:  # 以下这些配置都配置的nacos的配置中心里面，这样可以共享配置信息
    - rocketmq-config
    - nacos-config
    - redis-config
  cloud:
    nacos:
      config:
        group: xinyue-game
        file-extension: yaml  # 指定远程配置文件格式
        namespace: local-env
        prefix: xinyue-game
        server-addr: localhost:8848
```

业务配置application.yml默认可以不用修改，如果有业务变化，再根据需求修改即可。

#### 游戏示例登陆服务

在Eclipse中，打开FishingHunterLoginServer项目的config/boostrap.yml，添加nacos的服务信息与信息配置

```

spring:
  profiles:
    active:
    - rocketmq-config
    - nacos-config
    - redis-config
    - mongo-config
  cloud:
    nacos:
      config:
        group: xinyue-game
        file-extension: yaml  # 指定远程配置文件格式
        namespace: local-env
        prefix: xinyue-game
        server-addr: localhost:8848
```

配置applicaion.yml默认可以不用修改。

#### 游戏示例逻辑服务

在Eclipse中，打开FishingHunterLogic项目的config/bootstap.yml，添加nacos的服务信息与信息配置

```

spring:
  profiles:
    active:
    - rocketmq-config
    - nacos-config
    - redis-config
    - mongo-config
  cloud:
    nacos:
      config:
        group: xinyue-game
        file-extension: yaml  # 指定远程配置文件格式
        namespace: local-env
        prefix: xinyue-game
        server-addr: localhost:8848
```

配置application.yml默认可以不用修改。



### 游戏示例项目启动

#### 在Eclipse中启动

在各个服务中找到对应的启动类，直接运行即可。

1. 启动游戏中心服务，启动类：WebGameCenterServerMain
2. 启动Web网关服务，启动类：GameWebServerGatewayMain
3. 启动游戏网关服务，启动类：GameGatewayMain
4. 启动示例登陆服务，启动类：FishingHunterLoginServer
5. 启动示例业务服务，启动类：FishServerMain

#### 打包成Jar包运行

在需要运行的项目pom.xml所以的目录下面，执行下面的命令：

```
mvn clean package
```

在相应项目的target目录下就可以找到可以运行的jar包。

比如游戏示例的登陆服务，在FishingHunterLoginServer目录下执行 mvn clean package，target目录下就需要 生成：`FishingHunterLoginServer-0.0.1-SNAPSHOT.jar`，然后把它和config文件夹放在同一个目录下，使用下面的命令：

```
java -jar FishingHunterLoginServer-0.0.1-SNAPSHOT.jar
```


## 客户端

如果需要启动客户端，需要下载Unity，版本需要大于等于`2019.3.1f`


![欢迎关注](https://images.gitee.com/uploads/images/2020/0307/151917_ffb1d7f0_23677.png "QQ截图20191104223446.png")

QQ交流群：66728073 有问必答  更多信息可以关注公从号

 **谢赏** 

<hr>

![](https://images.gitee.com/uploads/images/2020/0307/154752_cfc15eaf_23677.png "zhifubaopay.png")![](https://images.gitee.com/uploads/images/2020/0307/154803_608eff87_23677.png "weixinpay.png")